# Getting Started

## Mini project (Open api 3.0 / Swagger exposing endpoints)

### Swagger endpoint : http://localhost:9090/swagger-ui.html

#### H2 database credentials :
* username : sa
* password : 123456

---------------------
### screenshots : 

![Screenshot](./src/main/resources/static/screenshots/swagger.png)
