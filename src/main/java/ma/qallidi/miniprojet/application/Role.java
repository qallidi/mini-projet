package ma.qallidi.miniprojet.application;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum Role {
    ADMIN("admin"),
    USER("user");

    public static final Random random = new Random();
    private static final List<Role> roles = Collections.unmodifiableList(Arrays.asList(values()));
    private static final int size = roles.size();
    public final String role;

    Role(String role) {
        this.role = role;
    }

    public static String valueOf(Role value) {
        return value.role;
    }

    /**
     * function to return a random role
     *
     * @return
     */
    public static Role getRandomRole() {
        return roles.get(random.nextInt(size));
    }


}
