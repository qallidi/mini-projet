package ma.qallidi.miniprojet.application.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Data
@Entity(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private String birthDate;
    @NotNull
    private String city;
    @NotNull
    private String country;
    @NotNull
    private String avatar;
    @NotNull
    private String company;
    @NotNull
    private String jobPosition;
    @NotNull
    private String mobile;
    @NotNull
    private String username;
    @NotNull
    private String email;
    @NotNull
    @Size(min = 6, max = 10)
    private String password;
    @NotNull
    private String role;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return username.equals(user.username) && email.equals(user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, email);
    }
}
