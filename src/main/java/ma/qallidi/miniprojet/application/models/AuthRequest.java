package ma.qallidi.miniprojet.application.models;

import lombok.Data;

@Data
public class AuthRequest {
    private String username;
    private String password;
}
