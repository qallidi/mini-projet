package ma.qallidi.miniprojet.application.models;

import lombok.Data;

@Data
public class AuthResponse {
    private String accessToken;
}
