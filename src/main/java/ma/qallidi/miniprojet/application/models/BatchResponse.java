package ma.qallidi.miniprojet.application.models;

import lombok.Data;

@Data
public class BatchResponse {
    private Integer TotalInputs;
    private Integer TotalSuccessInputs;
    private Integer TotalFailedInputs;
}
