package ma.qallidi.miniprojet.application.models;

import lombok.Data;
import ma.qallidi.miniprojet.application.entities.User;

import java.util.List;

@Data
public class InputResponse {
    private List<User> userList;
    private BatchResponse batchResponse;
}
