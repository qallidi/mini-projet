package ma.qallidi.miniprojet.application.models;

import lombok.Data;

@Data
public class UserModel {
    private String firstName;
    private String lastName;
    private String birthDate;
    private String city;
    private String country;
    private String avatar;
    private String company;
    private String jobPosition;
    private String mobile;
    private String username;
    private String email;
    private String password;
    private String role;
}
