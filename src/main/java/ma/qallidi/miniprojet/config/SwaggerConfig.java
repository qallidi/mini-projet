package ma.qallidi.miniprojet.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Value("${app.openapi.title}")
    private String title;
    @Value("${app.openapi.version}")
    private String version;
    @Value("${app.openapi.description}")
    private String description;
    @Value("${app.openapi.license.name}")
    private String licenseName;
    @Value("${app.openapi.license.url}")
    private String licenceUrl;
    @Value("${app.openapi.license.terms}")
    private String termsOfService;

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI().info(this.getInfos());
    }

    @Bean
    public GroupedOpenApi api() {
        return GroupedOpenApi.builder().group("api").pathsToMatch("/api/**").build();
    }

    /**
     *
     * @return
     */
    private Info getInfos() {
        return new Info().title(title).version(version).description(description).license(new License().name(licenseName).url(licenceUrl)).termsOfService(termsOfService);
    }
}
