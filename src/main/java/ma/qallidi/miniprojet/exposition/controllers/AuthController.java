package ma.qallidi.miniprojet.exposition.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import ma.qallidi.miniprojet.application.models.AuthRequest;
import ma.qallidi.miniprojet.application.models.AuthResponse;
import ma.qallidi.miniprojet.exposition.implementations.AuthServiceImp;
import ma.qallidi.miniprojet.exposition.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@Tag(name = "auth controller", description = "you will find auth endpoint here")
public class AuthController {

    private final AuthServiceImp authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @Operation(summary = "endpoint to generate JWT token", description = "you need to send your username/email & your password")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successfully generated"),
            @ApiResponse(responseCode = "404", description = "User Not Found Or bad credentials"),
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuthResponse> auth(@RequestBody AuthRequest request) {
        AuthResponse authResponse = this.authService.generateJWT(request);
        if (authResponse.getAccessToken() != null) return new ResponseEntity<>(authResponse, HttpStatus.OK);
        else return new ResponseEntity<>(authResponse, HttpStatus.NOT_FOUND);
    }

}
