package ma.qallidi.miniprojet.exposition.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import ma.qallidi.miniprojet.application.entities.User;
import ma.qallidi.miniprojet.application.models.BatchResponse;
import ma.qallidi.miniprojet.application.models.UserModel;
import ma.qallidi.miniprojet.exposition.implementations.UserServiceImp;
import ma.qallidi.miniprojet.exposition.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/users")
@Tag(name = "user controller", description = "you will find all user's endpoints")
public class UserController {
    private final UserServiceImp userService;

    @Autowired
    UserController(UserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "endpoint to generate users", description = "you need to specify the count number to generate (count) users")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successfully created"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error"),
            @ApiResponse(responseCode = "404", description = "Not Found"),
            @ApiResponse(responseCode = "401", description = "Unauthorized")
    })
    @GetMapping(value = "/generate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserModel>> generateUsers(@Parameter(name = "count", description = "the amount of user to be generated") @RequestParam(name = "count") Integer count) throws IOException {
        return userService.generateUsers(count);
    }

    @Operation(summary = "endpoint to save users", description = "you need to upload file or files to save users")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successfully saved"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error"),
            @ApiResponse(responseCode = "404", description = "Not Found"),
            @ApiResponse(responseCode = "308", description = "Resume Incomplete")
    })
    @PostMapping(value = "/batch", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BatchResponse> saveUsers(@RequestPart(name = "file") MultipartFile multipartFile) throws IOException {
        return userService.persistUsers(multipartFile);
    }

    @Operation(summary = "endpoint to find user info", description = "you need to send your token to get your info")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successfully created"),
            @ApiResponse(responseCode = "401", description = "Unauthorized")
    })
    @GetMapping(value = "/me", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> findMe(@RequestHeader(name = "Authorisation") String token) {
        if (!token.contains("Bearer")) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } else {
            User user = this.userService.getUserInfo(token);
            if (user != null) return new ResponseEntity<>(user, HttpStatus.OK);
            else return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @Operation(summary = "endpoint to find user info by username", description = "you need to send your token to get user info by username")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successfully created"),
            @ApiResponse(responseCode = "401", description = "Unauthorized")
    })
    @GetMapping(value = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> findUser(@RequestHeader(name = "Authorisation") String token, @PathVariable String username) {
        if (!token.contains("Bearer")) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } else {
            User user = this.userService.getUserInfo(token, username);
            if (user != null) return new ResponseEntity<>(user, HttpStatus.OK);
            else return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }
}
