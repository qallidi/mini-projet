package ma.qallidi.miniprojet.exposition.implementations;

import ma.qallidi.miniprojet.application.entities.User;
import ma.qallidi.miniprojet.application.models.AuthRequest;
import ma.qallidi.miniprojet.application.models.AuthResponse;

import java.util.Optional;

public interface AuthServiceImp {

    AuthResponse generateJWT(AuthRequest request);

    String jwtGenerator(User user);

    Optional<User> parseToken(String token);
}
