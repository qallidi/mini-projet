package ma.qallidi.miniprojet.exposition.implementations;

import com.github.javafaker.Faker;
import ma.qallidi.miniprojet.application.entities.User;
import ma.qallidi.miniprojet.application.models.BatchResponse;
import ma.qallidi.miniprojet.application.models.InputResponse;
import ma.qallidi.miniprojet.application.models.UserModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface UserServiceImp {

    ResponseEntity<List<UserModel>> generateUsers(Integer count) throws IOException;

    ResponseEntity<BatchResponse> persistUsers(MultipartFile multipartFile) throws IOException;

    List<UserModel> usersGenerator(Integer count);

    UserModel mappingFakeValues(Faker faker);

    List<User> extractUsers(MultipartFile file) throws IOException;

    User bCryptEncoder(User user);

    InputResponse filterByEmailAndUsername(List<User> userDBList, List<User> userFileList);

    User getUserInfo(String accessToken);

    User getUserInfo(String accessToken, String username);
}
