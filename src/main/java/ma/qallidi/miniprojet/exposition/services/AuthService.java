package ma.qallidi.miniprojet.exposition.services;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import ma.qallidi.miniprojet.application.entities.User;
import ma.qallidi.miniprojet.application.models.AuthRequest;
import ma.qallidi.miniprojet.application.models.AuthResponse;
import ma.qallidi.miniprojet.application.repositories.UserRepository;
import ma.qallidi.miniprojet.exposition.implementations.AuthServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class AuthService implements AuthServiceImp {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", Pattern.CASE_INSENSITIVE);
    private final UserRepository userRepository;
    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    public AuthService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * @param request
     * @return
     */
    @Override
    public AuthResponse generateJWT(AuthRequest request) {
        AuthResponse authResponse = new AuthResponse();
        String usernameOrEmail = request.getUsername();
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(usernameOrEmail);
        Optional<User> optionalUser;
        if (matcher.find()) optionalUser = this.userRepository.findUserByEmail(usernameOrEmail);
        else optionalUser = this.userRepository.findUserByUsername(usernameOrEmail);
        optionalUser.ifPresent(user -> authResponse.setAccessToken(this.jwtGenerator(user)));
        return authResponse;
    }

    /**
     * @param user
     * @return
     */
    @Override
    public String jwtGenerator(User user) {
        Claims claims = Jwts.claims().setSubject(user.getEmail());
        claims.put("userId", user.getId() + "");
        claims.put("firstname", user.getFirstName() + "");
        claims.put("lastname", user.getFirstName() + "");
        claims.put("role", user.getRole() + "");
        return "Bearer " + Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS256, secret).compact();
    }

    /**
     * @param token
     * @return
     */
    @Override
    public Optional<User> parseToken(String token) {
        String usernameOrEmail = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token.replace("Bearer", ""))
                .getBody()
                .getSubject();
        return this.userRepository.findUserByEmail(usernameOrEmail);
    }
}
