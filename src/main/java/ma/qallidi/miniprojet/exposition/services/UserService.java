package ma.qallidi.miniprojet.exposition.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import ma.qallidi.miniprojet.application.Role;
import ma.qallidi.miniprojet.application.entities.User;
import ma.qallidi.miniprojet.application.models.BatchResponse;
import ma.qallidi.miniprojet.application.models.InputResponse;
import ma.qallidi.miniprojet.application.models.UserModel;
import ma.qallidi.miniprojet.application.repositories.UserRepository;
import ma.qallidi.miniprojet.exposition.implementations.UserServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@Slf4j
public class UserService implements UserServiceImp {

    private static final PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
    private final UserRepository repository;
    private final AuthService authService;

    @Autowired
    UserService(UserRepository repository, AuthService authService) {
        this.repository = repository;
        this.authService = authService;
    }

    @Override
    public ResponseEntity<List<UserModel>> generateUsers(Integer count) {
        List<UserModel> results = this.usersGenerator(count);
        if (results.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<>(results, HttpStatus.OK);
        }
    }

    /**
     * @param multipartFile
     * @return
     * @throws IOException
     */
    @Override
    public ResponseEntity<BatchResponse> persistUsers(MultipartFile multipartFile) throws IOException {
        List<User> resultDB = this.repository.findAll();
        List<User> resultFile = this.extractUsers(multipartFile);
        InputResponse response = filterByEmailAndUsername(resultDB, resultFile);
        this.repository.saveAll(response.getUserList());
        return new ResponseEntity<>(response.getBatchResponse(), HttpStatus.OK);
    }


    /**
     * @param count
     * @return
     */
    @Override
    public List<UserModel> usersGenerator(Integer count) {
        return IntStream.range(0, count).mapToObj(index -> {
            Faker faker = new Faker(new Locale("en-US"));
            return mappingFakeValues(faker);
        }).collect(Collectors.toList());
    }

    /**
     * @param faker
     * @return
     */
    @Override
    public UserModel mappingFakeValues(Faker faker) {
        UserModel userModel = new UserModel();
        userModel.setFirstName(faker.name().firstName());
        userModel.setLastName(faker.name().lastName());
        userModel.setMobile(faker.phoneNumber().cellPhone());
        userModel.setAvatar(faker.avatar().image());
        userModel.setBirthDate(faker.date().birthday().toString());
        userModel.setEmail(faker.internet().emailAddress());
        userModel.setCountry(faker.country().name());
        userModel.setCity(faker.address().cityName());
        userModel.setCompany(faker.company().name());
        userModel.setJobPosition(faker.job().position());
        userModel.setUsername(faker.name().username());
        userModel.setPassword(faker.internet().password(6, 10, false, true, true));
        userModel.setRole(Role.getRandomRole().name().toLowerCase());
        return userModel;
    }

    /**
     * @param file
     * @return
     * @throws IOException
     */
    @Override
    public List<User> extractUsers(MultipartFile file) throws IOException {
        ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());
        TypeFactory typeFactory = mapper.getTypeFactory();
        CollectionType collectionType = typeFactory.constructCollectionType(List.class, User.class);
        List<User> users = mapper.readValue(file.getInputStream(), collectionType);
        return users.stream().map(this::bCryptEncoder).collect(Collectors.toList());
    }

    /**
     * @param user
     * @return
     */
    @Override
    public User bCryptEncoder(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        return user;
    }

    /**
     * @param userDBList
     * @param userFileList
     * @return
     */
    @Override
    public InputResponse filterByEmailAndUsername(List<User> userDBList, List<User> userFileList) {
        BatchResponse batchResponse = new BatchResponse();
        InputResponse response = new InputResponse();
        List<User> nonDuplicatedUsers = userFileList.stream().filter(user -> userDBList.stream().noneMatch(user1 -> user1.equals(user))).collect(Collectors.toList());
        batchResponse.setTotalSuccessInputs(nonDuplicatedUsers.size());
        batchResponse.setTotalFailedInputs(userFileList.size() - nonDuplicatedUsers.size());
        batchResponse.setTotalInputs(userDBList.size() + nonDuplicatedUsers.size());
        response.setUserList(nonDuplicatedUsers);
        response.setBatchResponse(batchResponse);
        return response;
    }

    @Override
    public User getUserInfo(String token) {
        Optional<User> userOptional = this.authService.parseToken(token);
        if (userOptional.isPresent()) {
            return userOptional.get();
        } else {
            return null;
        }
    }

    @Override
    public User getUserInfo(String token, String username) {
        Optional<User> userOptional = this.authService.parseToken(token);
        if (userOptional.isPresent() && Role.valueOf(Role.ADMIN).equals(userOptional.get().getRole())) {
            Optional<User> user = this.repository.findUserByUsername(username);
            if (user.isPresent())
                return user.get();
            else return null;
        } else {
            return null;
        }
    }
}
