package ma.qallidi.miniprojet.exposition.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.qallidi.miniprojet.application.entities.User;
import ma.qallidi.miniprojet.application.models.AuthRequest;
import ma.qallidi.miniprojet.application.models.AuthResponse;
import ma.qallidi.miniprojet.application.models.UserModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
class AuthServiceTest {
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final AuthRequest authRequest = new AuthRequest();
    private static UserModel testUser = new UserModel();
    private static User user = new User();
    @Autowired
    private UserService userService;
    @Autowired
    private AuthService authService;

    @BeforeEach
    void setUp() {
        testUser = userService.usersGenerator(1).get(0);
        authRequest.setUsername(testUser.getUsername());
        authRequest.setPassword(testUser.getPassword());
    }

    @Test
    @Disabled
    void generateJWT() {
        user = mapper.convertValue(testUser.toString(), User.class);
        String expectedToken = this.authService.jwtGenerator(user);
        AuthResponse response = this.authService.generateJWT(authRequest);
        assertEquals(expectedToken, response.getAccessToken());
    }

    @Test
    void jwtGenerator() {
    }

    @Test
    void parseToken() {
    }
}