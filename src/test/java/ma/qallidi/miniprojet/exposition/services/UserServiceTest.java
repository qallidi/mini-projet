package ma.qallidi.miniprojet.exposition.services;

import ma.qallidi.miniprojet.application.models.UserModel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class UserServiceTest {

    @Autowired
    private UserService userService;

    @DisplayName("users Generator function test")
    @Test
    void usersGenerator() {
        // call user service to generate an (x) amount of users
        Integer count = 8;
        List<UserModel> expectedList = this.userService.usersGenerator(count);
        assertEquals(8, expectedList.size());
    }

    @DisplayName("users Generator service test")
    @Test
    void generateUsers() {
        // call user service to generate an (x) amount of users and verify if HttpStatus is 200 OK
        Integer count = 13;
        ResponseEntity<List<UserModel>> response = this.userService.generateUsers(count);
        if (response.hasBody()) {
            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertEquals(13, Objects.requireNonNull(response.getBody()).size());
        } else assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }
}